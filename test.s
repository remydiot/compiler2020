			# This code was produced by the CERI Compiler
.data
FormatString1:	.string "%llu"	# used by printf to display 64-bit unsigned integers
FormatString2:	.string "%lf"	# used by printf to display 64-bit floating point numbers
FormatString3:	.string "%c"	# used by printf to display a 8-bit single character
TrueString:	.string "TRUE"	# used by printf to display the boolean value TRUE
FalseString:	.string "FALSE"	# used by printf to display the boolean value FALSE
a:	.quad 0
b:	.quad 0
i:	.quad 0
	.align 8
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $10
	pop i
For0:
	push $0
	pop %rax	# Get the result of expression
	cmpq %rax, i	#Variable renvoyée par AssignementStatement()
	je FinFor0	# if FALSE, jump out of the loop0
Case1:
	cmpq $11,i
	je CaseNumber1_1
	cmpq $10,i
	je CaseNumber1_1
	cmpq $9,i
	je CaseNumber1_1
	jmp Nextcas1_1
CaseNumber1_1:
	movq $0, %rax
	movb $'>',%al
	push %rax	# push a 64-bit version of '>'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	jmp FinCase1
Nextcas1_1:
	cmpq $8,i
	je CaseNumber1_2
	jmp Nextcas1_2
CaseNumber1_2:
	movq $0, %rax
	movb $'H',%al
	push %rax	# push a 64-bit version of 'H'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	jmp FinCase1
Nextcas1_2:
	cmpq $7,i
	je CaseNumber1_3
	jmp Nextcas1_3
CaseNumber1_3:
	movq $0, %rax
	movb $'E',%al
	push %rax	# push a 64-bit version of 'E'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	jmp FinCase1
Nextcas1_3:
	cmpq $6,i
	je CaseNumber1_4
	cmpq $5,i
	je CaseNumber1_4
	jmp Nextcas1_4
CaseNumber1_4:
	movq $0, %rax
	movb $'L',%al
	push %rax	# push a 64-bit version of 'L'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	jmp FinCase1
Nextcas1_4:
	cmpq $4,i
	je CaseNumber1_5
	jmp Nextcas1_5
CaseNumber1_5:
	movq $0, %rax
	movb $'O',%al
	push %rax	# push a 64-bit version of 'O'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	jmp FinCase1
Nextcas1_5:
	cmpq $1,i
	je CaseNumber1_6
	jmp Nextcas1_6
CaseNumber1_6:
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push $0
	pop b
For8:
	push $9
	pop %rax	# Get the result of expression
	cmpq %rax, b	#Variable renvoyée par AssignementStatement()
	je FinFor8	# if FALSE, jump out of the loop8
Case9:
	cmpq $2,b
	je CaseNumber9_1
	jmp Nextcas9_1
CaseNumber9_1:
	movq $0, %rax
	movb $'W',%al
	push %rax	# push a 64-bit version of 'W'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	jmp FinCase9
Nextcas9_1:
	cmpq $3,b
	je CaseNumber9_2
	jmp Nextcas9_2
CaseNumber9_2:
	movq $0, %rax
	movb $'O',%al
	push %rax	# push a 64-bit version of 'O'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	jmp FinCase9
Nextcas9_2:
	cmpq $4,b
	je CaseNumber9_3
	jmp Nextcas9_3
CaseNumber9_3:
	movq $0, %rax
	movb $'R',%al
	push %rax	# push a 64-bit version of 'R'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	jmp FinCase9
Nextcas9_3:
	cmpq $5,b
	je CaseNumber9_4
	jmp Nextcas9_4
CaseNumber9_4:
	movq $0, %rax
	movb $'L',%al
	push %rax	# push a 64-bit version of 'L'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	jmp FinCase9
Nextcas9_4:
	cmpq $6,b
	je CaseNumber9_5
	jmp Nextcas9_5
CaseNumber9_5:
	movq $0, %rax
	movb $'D',%al
	push %rax	# push a 64-bit version of 'D'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	jmp FinCase9
Nextcas9_5:
DefaultCase9:
	movq $0, %rax
	movb $'_',%al
	push %rax	# push a 64-bit version of '_'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
FinCase9:
	push b
	push $1
	pop %rbx
	pop %rax
	addq %rbx, %rax	# ADD
	push %rax
	pop b
	jmp For8
FinFor8:
	jmp FinCase1
Nextcas1_6:
DefaultCase1:
	movq $0, %rax
	movb $'<',%al
	push %rax	# push a 64-bit version of '<'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
FinCase1:
	push i
	push $1
	pop %rbx
	pop %rax
	subq %rbx, %rax	# SUB
	push %rax
	pop i
	jmp For0
FinFor0:
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
