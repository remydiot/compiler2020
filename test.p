VAR		a,b,i :	INTEGER.

BEGIN
	FOR i:=10 DOWNTO 0 DO
	BEGIN
	CASE i OF
		11..9 : DISPLAY '>';
		8 :		DISPLAY 'H';
		7 :		DISPLAY 'E';
		6,5:	DISPLAY 'L';
		4:		DISPLAY 'O';
		1:		BEGIN
				DISPLAY '\n';
				FOR b:=0 TO 9 DO				
				CASE b OF
					2 : DISPLAY 'W';
					3 : DISPLAY 'O';
					4 : DISPLAY 'R';
					5 : DISPLAY 'L';
					6 : DISPLAY 'D';
					ELSE 
						DISPLAY '_';
					END;
		ELSE
			DISPLAY '<';
		END;
	DISPLAY '\n'
END.
